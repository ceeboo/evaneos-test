export type CellStatus = 'untouched' | 'flagged' | 'dug' | 'detonated';
export type CellAction = 'dig' | 'flag';

export class Cell {
    private _bomb: boolean;
    private _flagged: boolean;
    private _dug: boolean;
    private _number: number;


    static withBomb(): Cell {
        return new Cell(true, false, false);
    }

    static withoutBomb(): Cell {
        return new Cell(false, false, false);
    }

    constructor(withBomb: boolean, flagged: boolean, dug: boolean, number: number = 0) {
        this._bomb = withBomb;
        this._flagged = flagged;
        this._dug = dug;
        this._number = number;
    }

    flag(): Cell {
        if (this._dug === true) {
            throw new Error('This cell has already been dug');
        }
        return new Cell(this._bomb, !this._flagged, this._dug, this._number);
    }

    dig(): Cell {
        return new Cell(this._bomb, false, true, this._number);
    }

    increaseNumber() {
        this._number = this._number + 1;
    }

    adjacentCells(cellKey: number, column: number) {
        if (cellKey % 10 === 0) return [cellKey + 1, cellKey + column, cellKey + column + 1, cellKey - column, cellKey - column + 1];
        else if (cellKey % 10 === 9) return [cellKey - 1, cellKey + column, cellKey + column - 1, cellKey - column, cellKey - column - 1];
        else return [cellKey + 1, cellKey - 1, cellKey + column - 1, cellKey + column, cellKey + column + 1, cellKey - column - 1, cellKey - column, cellKey - column + 1];
    }

    get detonated(): boolean {
        return this._bomb && this.dug;
    }

    get flagged(): boolean {
        return this._flagged;
    }

    get dug(): boolean {
        return this._dug;
    }

    get bomb(): boolean {
        return this._bomb;
    }

    get number(): number {
        return this._number;
    }

    get status(): CellStatus {
        if (this.detonated) {
            return 'detonated';
        }
        if (this.dug) {
            return 'dug';
        }
        if (this.flagged) {
            return 'flagged';
        }
        return 'untouched';
    }
}
