import { Cell, CellAction } from './Cell';

export type Cells = Array<Cell>;

export class Grid {
    [key: number]: number;
    private _column: number;
    private _cells: Cells;
    private _cellsSave: Cells;

    static generate(row: number, column: number, minesCount: number): Grid {
        const length = row * column;
        let cells: Cells = [];

        for (let i = 0; i < length; i++) {
            const cell = minesCount > i ? Cell.withBomb() : Cell.withoutBomb();
            cells.push(cell);
        }

        let index = -1;
        while (++index < length) {
            const rand = index + Math.floor(Math.random() * (length - index));
            const cell = cells[rand];

            cells[rand] = cells[index];
            cells[index] = cell;
        }

        // Calcul cells number of adjacent bombs
        cells.forEach((cell, key) => {
            if (cell.bomb === true) {
                const adjacentcells = cell.adjacentCells(key, column);

                for (const adjacent of adjacentcells) {
                    if (cells.includes(cells[adjacent]) === false) continue;

                    cells[adjacent].increaseNumber();
                };
            }
        });

        return new Grid(column, cells);
    }

    constructor(column: number, cells: Cells, cellsSave: Cells = []) {
        if (!Number.isInteger(column)) {
            throw new TypeError('column count must be an integer');
        }

        if (cells.length % column !== 0 || cells.length === 0) {
            throw new RangeError(
                'cell count must be dividable by column count'
            );
        }

        this._column = column;
        this._cells = cells;
        this._cellsSave = cellsSave;
    }

    [Symbol.iterator]() {
        return this._cells[Symbol.iterator]();
    }

    map(
        callbackfn: (value: Cell, index: number, array: Cell[]) => {},
        thisArg?: any
    ) {
        return this._cells.map(callbackfn);
    }

    cellByIndex(index: number): Cell | undefined {
        return this._cells[index];
    }

    cellByCoodinates(x: number, y: number): Cell | undefined {
        return this._cells[this._column * y + x];
    }

    sendActionToCell(cellIndex: number, action: CellAction): Grid {
        // Save cells before change
        const save = this._cells;

        // Dig all empty adjacent cells
        if (this._cells[cellIndex].bomb === false && this._cells[cellIndex].number === 0) {
            this.digAdjacent(cellIndex);
        }

        const cells = [...this._cells];
        const cell = cells[cellIndex];

        cells[cellIndex] = cell[action]();

        return new Grid(this._column, cells, save);
    }

    digAdjacent(cellIndex: number) {
        const adjacentCells = this._cells[cellIndex].adjacentCells(cellIndex, this._column);
        for (const adjacentIndex of adjacentCells) {
            const adjacentCell: Cell = this._cells[adjacentIndex];

            if (this._cells.includes(adjacentCell) === false) continue;

            if (adjacentCell.bomb === false && adjacentCell.number === 0 && adjacentCell.dug === false) {
                this._cells[adjacentIndex] = adjacentCell.dig();
                this.digAdjacent(adjacentIndex);
            }
        }
    }

    // undoAction() {
    //     return new Grid(this._column, this._cellsSave);
    // }

    get column() {
        return this._column;
    }
}
